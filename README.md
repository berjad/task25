Task 25 - Basic Sequelize App

- Create a basic Node app
- Install Sequelize and dotenv
- Connect to the World database (Sample Data from MySQL Installation) using Sequelize
- Use process.env for database credentials

- Write 3 functions that use Sequelize Raw Queries to:
    - Get all the countries
    - Get all the cities
    - Get all the languages