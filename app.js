const { Sequelize, QueryTypes } = require('sequelize');

if(process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

const sequelize = new Sequelize({
    database: process.env.DB_DATABASE,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: process.env.DB_DIALECT
})

async function connect() {
    try {
        await sequelize.authenticate();
        console.log('Connection established...');

        const countries = await sequelize.query('SELECT * FROM country LIMIT 10', {type: QueryTypes.SELECT});
        const cities = await sequelize.query('SELECT * FROM city LIMIT 10', {type: QueryTypes.SELECT});
        const languages = await sequelize.query('SELECT * FROM countrylanguage LIMIT 10', {type: QueryTypes.SELECT});
        
        // const countries = await sequelize.query('SELECT * FROM country LIMIT 10', {
        //     type: QueryTypes.SELECT
        // });

        // const countryNames = countries.map(country => country.Name);
        // const countryPopulation = countries.map(country => country.Population);

        // const searchResults = countries.filter(country => country.Name.indexOf());

        // console.log(countryPopulation);
        // console.log(searchResults);
        // console.log(countries);

        console.log(countries);
        console.log(cities);
        console.log(languages);

        await sequelize.close();
        
        }
    catch(e) {
        console.error(e);
    }
}

connect()